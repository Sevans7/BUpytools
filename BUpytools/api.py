"""
Miscellaneous methods for simplifying the python code-writing experience.
"""

def apply(x, fstr, *args, **kwargs):
    '''return x.fstr(*args, **kwargs), or x if x doesn't have an 'fstr' attribute.'''
    __tracebackhide__ = True
    # pop default if it was provided.
    doing_default = 'default' in kwargs
    if doing_default:
        default = kwargs.pop('default')
    # call x.fstr(*args, **kwargs)   # (kwargs with 'default' popped.)
    if hasattr(x, fstr):
        return getattr(x, fstr)(*args, **kwargs)
    elif doing_default:
        return default
    else:
        return x

def is_integer(x):
    return isinstance(x, int) or apply(x, 'is_integer', default=False)

def print_clear(N=80):
    '''clears current printed line of up to N characters, and returns cursor to beginning of the line.
    debugging: make sure to use print(..., end=''), else your print statement will go to the next line.
    '''
    print('\r'+ ' '*N +'\r',end='')

def debug_viewer(debug=False):
    '''returns a function which prints and shows things if debug, else does nothing.
    if converting objects to strings is non-trivial, use the format kwarg:
        Example:
            explain = debug_viewer(debug)
            # the next line will always convert x to string, even if debug is False:
            explain('Did a thing to: {}'.format(x))
            # the next line will hit the input string with .format(*[x]), only if debug is True:
            explain('Did a thing to: {}', format=[x])
        Note the format kwarg is only compatible when there is 1 arg entered, for simplicity.
    '''
    if debug:
        _view_f = view   # this is the function view() from above
        def print_and_view(*args, view=None, format=[], **kw):
            if len(args)>0:
                if len(args)==1:
                    print(args[0].format(*format), **kw)
                elif len(format) > 0:
                    raise TypeError('kwarg "format" only available when precisely 1 arg is entered to the debug_viewer.')
                else:
                    print(*args, **kw)
            if view is not None:
                _view_f(view)
        return print_and_view
    else:
        def do_nothing(*args, **kw):
            pass
        return do_nothing

def _pop_kw(kw, defaults):
    '''pops keys in defaults (a dict) from kw (a dict).
    returns dict of popped values (defaulting to vals in defaults if key is not in kw).
    '''
    result = dict()
    for key in defaults.keys():
        result[key] = kw.pop(key, defaults[key])
    return result

def value_from_aliases(*aliases):
    '''returns value from aliases.
    Precisely one of the aliases must be non-None, else raises ValueError.
    '''
    not_nones = [(a is not None) for a in aliases]
    Nvals     = sum(not_nones)
    if Nvals == 1:
        return aliases[next(i for (i, not_none) in enumerate(not_nones) if not_none)]
    else:
        raise ValueError('Expected one non-None value, but got {}!'.format(Nvals))