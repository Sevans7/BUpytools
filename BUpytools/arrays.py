"""
Convenience for numpy arrays.
"""

# import stuff from internal modules
from .imports import (
    ImportFailed,
)

# import external public modules
try:  # NUMPY
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

''' ----------------------------- Arrays ----------------------------- '''

def slicer_at_ax(slicer, ax):
    '''return tuple of slices which, when applied to an array, takes slice along axis number <ax>.
    slicer: a slice object, or integer, or tuple of integers.
        slice or integer  -> use slicer directly.
        tuple of integers -> use slice(*slicer).
    ax: a number (negative ax not supported here).
    '''
    try:
        slicer[0]
    except TypeError: #slicer is a slice or an integer.
        pass  
    else: #assume slicer is a tuple of integers.
        slicer = slice(*slicer)
    return (slice(None),)*ax + (slicer,)

def slice_at_ax(arr, slice, ax):
    '''slice arr (a numpy array) by applying slice at axis number <ax>.
    (negative ax is supported here).
    See also: np.take().
        However; slicing creates a view, while np.take always copies data,
        so slicing may be the better option in many cases.
    '''
    ax = ax_to_abs_ax(ax, arr=arr)
    slicer = slicer_at_ax(slice, ax)
    return arr[slicer]

def ax_to_abs_ax(ax, ndim=None, arr=None):
    '''get positive ax index for an array with ndim dimensions.
    ax: integer
        axis index. Can be positive or negative.
    ndim: positive integer, or None.
        number of dimensions of array which ax will be indexing.
        if None, use arr to determine ndim.
    arr: array, or None.
        array which ax will be indexing (or array with same number of dimensions).
        use arr to calculate ndim via np.ndim(arr).
        if arr and ndim are None and ax is negative, raise ValueError.
    returns ax as a positive index.
    '''
    if ax >= 0:
        return ax
    # else
    if ndim is None:
        if arr is None:
            raise ValueError(f'must provide ndim or arr when ax<0. Got ax={ax}.')
        else:
            ndim = np.ndim(arr)
    else:
        assert arr is None, f"Provide only one of (arr, ndim); not both."
    return np.arange(ndim)[ax]

def slicer_by_value(arr, keep=lambda arr: np.ones(arr.shape, dtype=bool)):
    '''returns slice object which keeps only the appropriate values if applied to arr.
    arr: 1D array.
        >1D may be handled in the future but is not yet implemented.
    keep: function from array to boolean array.
        arr[result] will be only the values where keep == True.
        if the keep == True region of the array is not contiguous, raise ValueError.
        [TODO] handle "frequency" slicing case. E.g. [True, False, True, False] --> slice(A, B, 2)

    Examples:
        arr    = np.arange(10)
        keep   = lambda arr: (arr > 3) & (arr < 7)
        result = slicer_by_value(arr, keep)
        >>> slice (4, 7)
        arr[result]
        >>> array([4, 5, 6])
        
        keep2  = lambda arr: (arr < 3) | (arr > 7)
        slicer_by_value(arr, keep2)
        >>> ValueError      # there are multiple regions where keep2(arr) == True.
    '''
    arr   = np.asanyarray(arr)
    if arr.ndim != 1:
        raise NotImplementedError('slicer_by_value(arr) for arr with dimension != 1')
    kept  = keep(arr)
    where = np.where(kept)[0]
    result = slice(where[0], where[-1]+1)    # slice from first True to last True.
    # ensure the keep == True region of arr is contiguous before returning.
    if np.all(kept[result]):
        return result
    else:
        raise ValueError('non-contiguous region kept. Cannot represent keep(arr) as one slice')

def slicer_by_minmax(arr, min_=None, max_=None):
    '''returns slice object; if applied to arr, gives all values between min_ and max_, inclusive.
    arr: 1D array.
        >1D may be handled in the future but is not yet implemented.
    min_: None or value
        if provided, every value in arr[result] will be >= min_
    max_: None or value
        if provided, every value in arr[result] will be <= max_
    '''
    if min_ is None:
        if max_ is None:
            return slice(None, None, None)
        else:
            keep = lambda arr_: arr_ <= max_
    else:
        if max_ is None:
            keep = lambda arr_: min_ <= arr_
        else:
            keep = lambda arr_: (min_ <= arr_) & (arr_ <= max_)
    return slicer_by_value(arr, keep)

def argmax(arr):
    '''return idx where max is located.
    i.e. np.unravel_index(np.argmax(arr), np.shape(arr)),
    '''
    return np.unravel_index(np.argmax(arr), np.shape(arr))

def cmax(arr, *coords):
    '''return max, idxmax, coordmax.'''
    a = argmax(arr)
    return (arr[a], a, tuple((c[a[i]] for i, c in enumerate(coords))))

def variance_sqrt(x, axes=(0,1,2)):
    '''return sqrt(mean_i(  (xi - mean(x))**2  ))'''
    means = np.mean(x, axis=axes, keepdims=True)
    return np.sqrt(np.mean( (x - means)**2 , axis=axes) )

def stats(arr, advanced=True, finite_only=True):
    '''return dict with min, mean, max.
    if advanced, also include:
        std, median, size, number of non-finite points (e.g. np.inf or np.nan).
    if finite_only:
        only treat the finite parts of arr; ignore nans and infs.
    '''
    arr = arr_orig = np.asanyarray(arr)
    if finite_only or advanced:  # then we need to know np.isfinite(arr)
        finite = np.isfinite(arr)
        n_nonfinite = arr.size - np.count_nonzero(finite)
    if finite_only and n_nonfinite > 0:
        arr = arr[finite]
    result = dict(min=np.nanmin(arr), mean=np.nanmean(arr), max=np.nanmax(arr))
    if advanced:
        result.update(dict(std=np.nanstd(arr), median=np.nanmedian(arr),
                           size=arr.size, nonfinite=n_nonfinite))
    return result

def print_stats(arr_or_stats, advanced=True, fmt='{: .2e}', sep=' | ', return_str=False):
    '''calculate and prettyprint stats about array.
    arr_or_stats: dict (stats) or array-like.
        dict --> treat dict as stats of array.
        array --> calculate stats(arr, advanced=advanced)
    fmt: str
        format string for each stat.
    sep: str
        separator string between each stat.
    return_str: bool
        whether to return string instead of printing.
    '''
    fmtkey = '{:>6s}' if '\n' in sep else '{}'
    _stats = arr_or_stats if isinstance(arr_or_stats, dict) else stats(arr_or_stats, advanced=advanced)
    result = sep.join([f'{fmtkey.format(key)}: {fmt.format(val)}' for key, val in _stats.items()])
    return result if return_str else print(result)

def finite_op(arr, op):
    '''returns op(arr), hitting only the finite values of arr.
    if arr has only finite values,
        finite_op(arr, op) == op(arr).
    if arr has some nonfinite values (infs or nans),
        finite_op(arr, op) == op(arr[np.isfinite(arr)])
    '''
    arr = np.asanyarray(arr)
    finite = np.isfinite(arr)
    if np.count_nonzero(finite) < finite.size:
        return op(arr[finite])
    else:
        return op(arr)

def finite_min(arr):
    '''returns min of all the finite values of arr.'''
    return finite_op(arr, np.min)

def finite_mean(arr):
    '''returns mean of all the finite values of arr.'''
    return finite_op(arr, np.mean)

def finite_max(arr):
    '''returns max of all the finite values of arr.'''
    return finite_op(arr, np.max)

def finite_std(arr):
    '''returns std of all the finite values of arr.'''
    return finite_op(arr, np.std)

def finite_median(arr):
    '''returns median of all the finite values of arr.'''
    return finite_op(arr, np.median)

def rolling_mean(arr, N, axis=-1):
    '''rolling mean over N entries. result[i] = mean(arr[i:i+N]).
    N: int
        number of entries over which to take rolling mean.
    axis: index of axis; default -1.
        take rolling mean along the given axis.
    returns array of shape similar to arr, but with (N-1) fewer entries along <axis>.
    '''
    arr    = np.insert(arr, 0, 0, axis=axis)
    cumsum = np.cumsum(arr, axis=axis)
    cumsum_i_plus_N = slice_at_ax(cumsum, slice(N,None), axis)
    cumsum_i_minus1 = slice_at_ax(cumsum, slice(None,-N), axis)
    avg = (cumsum_i_plus_N - cumsum_i_minus1) / N
    return avg

def pairwise_mean(arr, axis=-1):
    '''return pairwise mean of arr, along axis. result[i] = (arr[i] + arr[i+1])/2
    axis: index of axis; default -1.
        take pairwise mean along the given axis.
    mathmatically equivalent to rolling_mean(arr, 2), but simpler / more efficient.
    returns array of shape similat to arr, but with 1 fewer entries along <axis>.
    '''
    arr_i_plus_1 = slice_at_ax(arr, slice(1, None), axis)
    arr_i        = slice_at_ax(arr, slice(None, -1), axis)
    return (arr_i_plus_1 + arr_i) / 2

def centered_grid(shape):
    '''returns a centered grid with the input shape.
    Example: shape = (3, 5)
    result will be a grid with shape (2, 3, 5),
        result[0] will be the x coordinates at each gridpoint,
        result[1] will be the y coordinates at each gridpoint.
        result[0][i][J] will be [-1, 0, 1] for any J.
        result[1][I][j] will be [-2, -1, 0, 1, 2] for any I.
    '''
    try:
        iter(shape)
    except TypeError:
        shape = [shape]
    shape = np.asarray(shape)
    slices = tuple(slice(0, s) for s in shape)
    grid = np.mgrid[slices]
    shift = np.expand_dims((shape - 1) / 2, tuple(i+1 for i in range(len(shape))))
    result = grid - shift
    return result

def gaussian_kernel(shape, sigma=1, mean=0, A=1, return_grid=False):
    '''makes a gaussian kernel with the given shape.
    The kernel will be normalized to have sum equal to A.
    '''
    if mean!=0:
        raise NotImplementedError('Gaussian kernel with nonzero mean')
    grid = centered_grid(shape)
    r2 = np.sum(grid**2, axis=0)
    kernel = np.exp(-0.5 * r2 / (sigma**2))
    result = A * kernel / kernel.sum()
    return (result, grid) if return_grid else result


''' ----------------------------- Matrices ----------------------------- '''
# Operations connected closely to "linear algebra".

def block_select_simple(M, rlim, clim):
    '''select blocks from M. Returns:
        [[ M[rslice, cslice], M[rslice, ^cslice] ],
         [ M[^rslice, cslice], M[^rslice, ^cslice] ]]
    where the notation ^slice means "take all indices except those indicated by slice",
    and rslice:=slice(rlim[0], rlim[1]); cslice:=slice(clim[0], clim[1]).
    In "plain english", the output means:
        [[ selected rows and columns,                  selected rows (excluding selected columns)],
         [ (excluding selected rows) selected columns, everything outside the selected regions   ]]
    For M > 2D, the indexing is applied along the final two axes.

    rlim, clim: 2-tuples
        determines slicing of M:
        rslice:=slice(rlim[0], rlim[1])
        cslice:=slice(clim[0], clim[1])
    '''
    M = np.asanyarray(M)
    r, R = rlim
    c, C = clim
    # handle "None" index:
    if r is None: r = 0
    if c is None: c = 0
    if R is None: R = M.shape[-2]
    if C is None: C = M.shape[-1]
    # make blocks:
    block_0_0 = M[..., r:R, c:C]
    block_0_1 = np.block([ M[..., r:R, 0:c], M[..., r:R, C: ] ])
    block_1_0 = np.block([ [M[..., 0:r, c:C]],
                           [M[..., R: , c:C]] ])
    block_1_1 = np.block([ [M[..., 0:r, 0:c], M[..., 0:r, C: ]],
                           [M[..., R: , 0:c], M[..., R: , C: ]] ])
    # return result:
    result = [[block_0_0, block_0_1],
              [block_1_0, block_1_1]]
    return result

def block_select_vector(V, lim):
    '''selects blocks from V. Returns:
        [ V[inside limits], V[outside limits] ]
    for V > 1D, the indexing is applied along the last axis.

    lim: 2-tuple
        determines slicing of V.
    '''
    V = np.asanyarray(V)
    l, L = lim
    # handle "None" index:
    if l is None: l = 0
    if L is None: L = V.shape[-1]
    # make blocks
    block_0 = V[..., l:L]
    block_1 = np.concatenate([V[..., 0:l], V[..., L: ]], axis=-1)
    # return result
    result = [block_0, block_1]
    return result

def linsys_pop_equations(M, C, ipop_lims, xpop):
    '''for M x + C = 0, pop the equations indexed by ipop_lims.

    M x + C = 0 represents a system of m equations in m variables.
    We will pop the equations indexed by ipop_lims (let's say there are p of them).
    Then we will have a system of (m - p) equations in (m - p) variables.

    INPUTS
    ------
    M: shape (m, m) array
        matrix of coefficients which apply to variable x.
        (E.g. maybe we are trying to solve this system for x.)
    C: shape (m,) array
        array of values which are "constant" with respect to x
    ipop_lims: 2-tuple (imin, imax)
        tells which rows to pop from the system of equations: pop equations[imin:imax]
        i.e. pop all equations from imin (inclusive) to imax (exclusive).
    xpop: shape (p,) array
        the values of x for the equations being popped.

    FURTHER EXPLANATION
    -------------------
    We can think of the transformation like:
    rewrite M x + C = 0 as:
        //                          \\  //        \\     //         \\
        | M{pop,pop}   M{pop,keep}   |  |  x{pop}  |  +  |  C{pop}   |
        | M{keep, pop} M{keep, keep} |  |  x{keep} |     |  C{keep}  |  =  0
        \\                          //  \\        //     \\         //
    Considering only the rows {keep}, yields (after matrix multiplication):
        M{keep, pop} x{pop} + M{keep, keep} x{keep} + C{keep} = 0
    So the new equation will be:
        M' x' + C' = 0, where:
            M' = M{keep, keep}
            x' = x{keep}
            C' = C{keep} + M{keep, pop} * x{pop}

    RETURNS (M', C')
    '''
    [[Mpp, Mpk], [Mkp, Mkk]] = block_select_simple(M, ipop_lims, ipop_lims)
    Cp, Ck = block_select_vector(C, ipop_lims)
    Mnew = Mkk
    Cnew = Ck + np.dot(Mkp, xpop)
    return (Mnew, Cnew)