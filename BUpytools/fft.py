"""
Fourier Transforms.
"""

# import stuff from internal modules
from .arrays import (
    finite_min, finite_max,
)
from .imports import (
    ImportFailed,
)
from .plots import (
    extent,
)
from .units import (
    pick_units,
)

# import external public modules
try:  # NUMPY
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')
try:  # MATPLOTLIB
    import matplotlib.pyplot as plt
except ImportError:
    plt = ImportFailed('matplotlib')


''' ----------------------------- Fourier Transforms 2D ----------------------------- '''

def fft2(values, xarr, yarr):
    '''calculates fft in 2d of values.
    xarr and yarr are the physical coordinates in x and y.
    
    returns fft(values), x-frequency, y-frequency.
    all will be shifted using np.fft.fftshift such that the middle is at (0,0).
    '''
    def dspace(xarr):
        return (xarr.max() - xarr.min()) / (xarr.size - 1)
    dx = dspace(xarr)
    dy = dspace(yarr)
    fx = np.fft.fftshift(np.fft.fftfreq(xarr.size, dx))
    fy = np.fft.fftshift(np.fft.fftfreq(yarr.size, dy))
    fvalues = np.fft.fftshift(np.fft.fft2(values))
    return fvalues, fx, fy

def fft2plot(fvalues, fx, fy, log=False, origin='lower', squaring=False,
             vmax_mul=1, vmin_mul=1, vmax_sub=0, vmin_add=0, vlim_diff=None, vmin=None, vmax=None,
             **kw_imshow):
    '''plot abs(fvalues) using imshow.
    fx and fy are used to set extent.
    if squaring, square values first.
    if log, plot log10 of the values instead.

    vmax_mul and vmin_mul will multiply the max and min of plotvals.
    vlim_diff, if provided, sets vmin = vmax - vlim_diff.

    NOTICE: you can use plt.xlim and ylim to zoom in on an imshow plot!
    '''
    plotvals = np.abs(fvalues)
    if squaring:
        plotvals = plotvals ** 2
    if log:
        plotvals = np.log10(plotvals)

    if vmax is None:
        vmax = finite_max(plotvals)
    vmax = vmax_mul * vmax - vmax_sub
    if vlim_diff is not None:
        if vmin is not None:
            raise ValueError('cannot provide vlim_diff AND vmin. vlim_diff sets vmin...')
        vmin = vmax - vlim_diff
    elif vmin is None:
        vmin = finite_min(plotvals)
    vmin = vmin_mul * vmin + vmin_add

    plt.imshow(plotvals, extent=extent(fx, fy), origin=origin, vmin=vmin, vmax=vmax, **kw_imshow)

def fft2plot_ez(values, xarr, yarr, **kw):
    '''fft2plot of *fft2(values, xarr, yarr)'''
    result = fft2(values, xarr, yarr)
    fft2plot(*result, **kw)
    return result

def fft2plot_space(fvalues, fx, fy, force_meters=True, squaring=False, **kw):
    '''fft2plot, also choosing nice units, and labeling well.
    fx and fy should be in [meters].

    if force_meters, plot will be in meters.
    Otherwise, we will try to pick good units.
    '''
    if force_meters:
        fx_prefix, fx_value = '', fx
        fy_prefix, fy_value = '', fy
    else:
        _, fx_prefix, fx_value = pick_units(fx)
        _, fy_prefix, fy_value = pick_units(fy)
    kx = 2 * np.pi * fx_value
    ky = 2 * np.pi * fy_value
    fft2plot(fvalues, kx, ky, squaring=squaring, **kw)
    plt.xlabel(r'$\vec{k}_x \; ['+fx_prefix+r'm^{-1}]$')
    plt.ylabel(r'$\vec{k}_y \; ['+fy_prefix+r'm^{-1}]$')
    return fvalues, kx, ky

def fft2plot_space_ez(values, xarr, yarr, **kw):
    '''fft2plot_space of *fft2(values, xarr, yarr)'''
    return fft2plot_space(*fft2(values, xarr, yarr), **kw)

''' ----------------------------- Fourier Transforms ND ----------------------------- '''

def fftN(values, *coord_arrays, **kw__fftn):
    '''calculates fft in 2d of values.
    coord_arrays are the physical coordinates along each dimension of values.
    E.g. (arr, xvalues, yvalues, tvalues) for arr a 3d array with x along dimension 1, y dim 2, t dim 3.
    
    returns fftn(values, **kw__fftn), coord0-frequency, coord1-frequency, ..., coordN-frequency.
    all frequencies will be shifted using np.fft.fftshift such that the middle is at (0,0).
    '''
    def dspace(xarr):
        return (xarr.max() - xarr.min()) / (xarr.size - 1)
    dcoords = tuple(dspace(carr) for carr in coord_arrays)
    freqs = tuple(np.fft.fftshift(np.fft.fftfreq(arr.size, dcoord)) for arr, dcoord in zip(coord_arrays, dcoords))
    fvalues = np.fft.fftshift(np.fft.fftn(values, **kw__fftn))
    return (fvalues, *freqs)
