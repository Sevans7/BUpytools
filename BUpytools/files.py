"""
Working with files / directories.
"""

# import built-in modules
import os
import functools
import itertools


def symlink(src, newfile, relpath=True):
    '''os.symlink, but with option to make link path be relative.
    src    : string; link will point AT this file.
    newfile: string; this file will be created.
    relpath: True (default) or False
        True  -> dst stores a relative path to src.
                 if dst and src are moved, but not moved relative to each other, link should still work.
        False -> dst stores an absolute path to src.
                 if dst is moved, link should still work. If src is moved, link will break.
    '''
    link = os.path.relpath(src, os.path.dirname(newfile)) if relpath else os.path.abspath(src)
    return os.symlink(link, newfile)

class EnterDir:
    '''context manager for remembering directory.
    upon enter, cd to directory (default os.curdir, i.e. no change in directory)
    upon exit, original working directory will be restored.

    For function decorator, see QOL.maintain_cwd.
    '''
    def __init__(self, directory=os.curdir):
        self.cwd       = os.path.abspath(os.getcwd())
        self.directory = directory

    def __enter__ (self):
        os.chdir(self.directory)

    def __exit__ (self, exc_type, exc_value, traceback):
        os.chdir(self.cwd)

RememberDir    = EnterDir  #alias
EnterDirectory = EnterDir  #alias

def with_dir(directory):
    '''returns a function decorator which:
    - changes current directory to <directory>.
    - runs function
    - changes back to original directory.
    '''
    def decorator(f):
        @functools.wraps(f)
        def f_but_enter_dir(*args, **kwargs):
            with EnterDir(directory):
                return f(*args, **kwargs)
        return f_but_enter_dir
    return decorator

withdir = with_dir #alias

# define a new function decorator, maintain_cwd, which maintains current directory:
maintain_cwd = with_dir(os.curdir)

maintain_directory = maintain_cwd  # alias
maintain_dir       = maintain_cwd  # alias

def next_available_filename(filename, sep='_', start=1, fill=False):
    '''returns next available filename, in local directory.

    Files will be named like:
        <filename><sep><N><ext>
    where N is a number, or ''.
    E.g. for sep='_', filename='Untitled', start=1, gives files in this order:
        'Untitled', 'Untitled_1', 'Untitled_2', ...

    fill: False (default) or True
        False --> look for the last existing name, and go one more than it.
            If folder has ['Untitled', 'Untitled_5'], return 'Untitled_6'.
        True  --> look for first available name, allowing to fill holes.
            If folder has ['Untitled', 'Untitled_5'], return 'Untitled_1'.

    All names above have extension implicit at the end. E.g. 'Untitled_5.txt'.
    '''
    if not os.path.lexists(filename):
        return filename
    base, input_ext = os.path.splitext(filename)
    existing_file_nums = []
    files  = os.listdir()
    for file in files:
        has_prefix, has_base, info3 = file.partition(base)
        if (has_base) and (not has_prefix):
            info2, file_ext = os.path.splitext(info3)
            if file_ext == input_ext:
                if len(info2)==0:
                    i = ''
                else:
                    has_prefix, has_sep, file_num = info2.partition(sep)
                    if (has_sep) and (not has_prefix):
                        try:
                            i = int(file_num)
                        except ValueError:
                            continue  # skip this file.
                    else:
                        continue
                existing_file_nums.append(i)
    if fill:
        for num in itertools.chain([''], itertools.count(start)):
            if num not in existing_file_nums:
                break  # use this num. See return statement below.
    else:
        sortkey = lambda num: (start - 1) if (num=='') else int(num)
        sorted_file_nums = sorted(existing_file_nums, key=sortkey)
        num = sorted_file_nums[-1]
    if num == '':
        return base + sep + str(start) + input_ext
    else:
        return base + sep + str(int(num)+1) + input_ext