"""
Convenience for importing modules.
- Handling import errors gracefully.
- Reloading modules.
"""

# import built-in modules
import sys
import importlib
import warnings

# import stuff from internal modules
from .lists import (
    sort_by_priorities,
)

# defaults
IMPORT_FAILURE_WARNINGS = False    # whether to warn (immediately) when an optional module fails to import.
    # Either way, we will still raise ImportFailedError upon accessing a module which failed to import.


''' --------------------------- import error handling --------------------------- '''

class ImportFailedError(ImportError):
    pass

class ImportFailed():
    '''set modules which fail to import to be instances of this class;
    initialize with modulename, additional_error_message.
    Then, when attempting to access any attribute of the ImportFailed object,
    raises ImportFailedError('. '.join(modulename, additional_error_message)).
    Also, if IMPORT_FAILURE_WARNINGS, make warning immediately when initialized.

    Example:
    try:
        import zarr
    except ImportError:
        zarr = ImportFailed('zarr', 'This module is required for compressing data.')

    zarr.load(...)   # << attempt to use zarr by accessing any of its attributes.
    # if zarr was imported successfully, it will work fine.
    # if zarr failed to import, this error will be raised:
    >>> ImportFailedError: zarr. This module is required for compressing data.
    '''
    def __init__(self, modulename, additional_error_message=''):
        self.modulename = modulename
        self.additional_error_message = additional_error_message
        if IMPORT_FAILURE_WARNINGS:
            warnings.warn(f'Failed to import module {modulename}.{additional_error_message}')

    def __getattr__(self, attr):
        str_add = str(self.additional_error_message)
        if len(str_add) > 0:
            str_add = '. ' + str_add
        raise ImportFailedError(self.modulename + str_add)

# old function. Recommend to use ImportFailed() instead.
def generate_import_warning(modulename, category=Warning,
                            message='Failed to import {module}. Functions relying on {module} may fail.',
                            add_message=''):
    '''raises (via warnings.warn()) and returns a warning that modulename failed to import.
    The warning message will be be (' '.join(message, add_message)).format(module=modulename)

    Example Usage:
        try:
            import pandas as pd
        except:
            pd = generate_import_warning('pandas')
    '''
    failed_import_warning = category(message.format(module=modulename))
    warnings.warn(failed_import_warning)  # warn user that the module failed to import.
    return failed_import_warning


''' --------------------------- reloading modules --------------------------- '''

def reload(package='BUpytools', prioritize=[], de_prioritize=[]):
    '''reloads all modules from package which have been imported.
    prioritize modules containing any strings in the list prioritize.
    de_prioritize modules containing any strings in the list de_prioritize.
    returns list of names of reloaded modules.
    '''
    reloaded = []
    to_reload = [(name, module) for (name, module) in sys.modules.items() if name.startswith(package)]
    to_reload = sort_by_priorities(to_reload, prioritize, de_prioritize,
                                   equals=lambda priority, name_and_module: priority in name_and_module[0])
    for (name, module) in to_reload:
        importlib.reload(module)
        reloaded += [name]
    return reloaded