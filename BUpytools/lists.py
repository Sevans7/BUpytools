"""
Convenience for lists, tuples, iterables. (For numpy arrays, see arrays.py instead)
"""

# import built-in modules
import itertools

## NOTE: IMPORTS.PY DEPENDS ON THIS MODULE. 
## DO NOT IMPORT ANY INTERNAL MODULES HERE WHICH RELY ON IMPORTS.PY.


def argsort(x, reverse=False, key=lambda y: y):
    '''does an argsort using pythons builtin function: sorted'''
    return [ix[0] for ix in sorted(zip(range(len(x)), x), key=lambda ix: key(ix[1]), reverse=reverse)]

def find(x, element, default=None, equals=lambda v1, v2: v1==v2):
    '''find smallest index i for which equals(x[i], element);
    return None if no such i exists.
    '''
    try:
        return next(i for i, elem in enumerate(x) if equals(elem, element))
    except StopIteration:
        return default

def product_dict(x):
    '''(Iterator for) cartesian product of values in dict x. See itertools.product for more details.

    e.g. product_dict( dict(n=[1,2,3], l=['a','b'], x=[8, 9]) ) -->
        dict(n=1, l='a', x=8), dict(n=1, l='a', x=9), dict(n=1, l='b', x=8), dict(n=1, l='b', x=9),
        dict(n=2, l='a', x=8), dict(n=2, l='a', x=9), dict(n=2, l='b', x=8), dict(n=2, l='b', x=9),
        dict(n=3, l='a', x=8), dict(n=3, l='a', x=9), dict(n=3, l='b', x=8), dict(n=3, l='b', x=9)

    values in x must all be finite iterables (e.g. lists).

    returns iterator which yields dict keys=x.keys(), values= single choices from each of x.values().
    '''
    keys, values = list(  zip( *(x.items()) )  )
    for i, val in enumerate(values):
        try:
            iter(val)
        except TypeError:
            raise TypeError('All values must be iterable! Failed at {}={}'.format(keys[i], val))
    for p in itertools.product(*values):
        yield {key : p[i] for i, key in enumerate(keys)}

def product_dict_length(x):
    '''number of terms in the cartesian product of values (which must be finite iterables) in dict x.
    e.g. product_dict_length( dict(n=[1,2,3], l=['a','b'], x=[8, 9]) ) == 3 * 2 * 2 == 12.
    '''
    result = 1
    for val in x.values():
        result *= len(list(val))
    return result

def product_list(*lists, as_string=False):
    '''returns list of elements in the cartesian product of args.
    Each arg should be a finite iterable (e.g. a list).

    Example:
        product_list([1,2,3],['x', 'y']) # --> 1x 1y 2x 2y 3x 3y
        # returns: [[1,'x'],[1,'y'],[2,'x'],[2,'y'],[3,'x'],[3,'y']]

    as_string: False (default) or True
        False -> returns the list as-is.
        True  -> convert all elements to strings then join in a reasonable way,
                 e.g. [['1e15', '1e10'], ['2e7', '3e5']] --> ['[1e15, 1e20]', '[2e7, 3e5]']
    '''
    result = []
    for p in itertools.product(*lists):
        if as_string:
            p = strlist(p)
        else:
            p = list(p)
        result.append(p)
    return result

def compose(*funcs):
    '''makes composite function from all the (arbitrary number of) functions provided.
    Not fully generalized; assumes each function takes and outputs exactly one argument.
    compose(f, g, h) --> lambda x: f( g( h( x ) ) ).

    TODO: generalize via kwargs wrap and unwrap: h(x) instead becomes wrap(h(unwrap(x)))

    __name__ of returned function reflects input function __names__.
    compose(f, g, h).__name__ = 'f__of__g__of__h'.

    returns composite function. 
    '''
    if len(funcs)==0:
        identity = lambda x: x
        identity.__name__ = ''  #for nice __name__ of result.
        return identity
    else:
        fi = compose(*funcs[1:])
        ni = fi.__name__
        f0 = lambda x: funcs[0](fi(x))
        #make nice __name__ (if not tracking __name__, could just return f0 at this line.):
        try:
            n0 = funcs[0].__name__
        except AttributeError:
            n0 = 'unnamedf'
        if ni == '':
            f0.__name__ = n0
        else:
            f0.__name__ = n0+'__of__'+ni
        return f0


def sort_by_priorities(x, prioritize=[], de_prioritize=[], equals=lambda v1, v2: v1==v2):
    '''returns list of elements of x, reordered according to priorities.
    puts p in prioritize first (in order of prioritize) for any p which appear in x.
    puts p in de_prioritize last (de_prioritize[-1] goes at very end) for any p which appear in x.
    If x is infinite, program will stall.

    The equals key can be used to provide a custom "equals" function.
    For example, to prioritize any elements of x containing 'MEFIRST', you could do:
        sort_by_priorities(x, ['MEFIRST'], equals=lambda sp, sx: sp in sx)
    (Note that the second arg passed to equals will be the element of x.)
    '''
    start  = []
    middle = []
    end    = []
    for y in x:
        i = find(prioritize, y, default=None, equals=equals)
        if i is not None:
            start  += [(y, i)]
        else:
            j = find(de_prioritize, y, default=None, equals=equals)
            if j is not None:
                end += [(y, j)]
            else:
                middle += [y]
    # sort start and end
    start = [start[i][0] for i in argsort(start, key=lambda y_i: y_i[1])]
    end   = [end[i][0]   for i in argsort(end,   key=lambda y_i: y_i[1])]
    # return result
    return start + middle + end
