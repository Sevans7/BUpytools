"""
Helps with making nice plots.
"""
import os


from .imports import ImportFailed

try:  # NUMPY
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')
try:  # MATPLOTLIB
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from matplotlib import colors, ticker
    from matplotlib.animation import FuncAnimation
except ImportError:
    plt = mpl = colors = ticker = ImportFailed('matplotlib')

# import stuff from internal modules
from .sentinels import NO_VALUE
from .strings import (
    sci_notation, _remove_signs_from_0s
)
from .timing import ProgressUpdatePrinter
from .arrays import (
    finite_min, finite_max,
)


''' ----------------------------- Plots ----------------------------- '''

def figure_is_empty(fig):
    '''returns whether fig is "empty" (i.e. has no children).'''
    return len(fig.get_children()) <= 1  # 1 is for the background patch.

def current_figure_is_empty():
    '''returns whether the current figure is "empty".'''
    return figure_is_empty(plt.gcf())

active_figure_is_empty = current_figure_is_empty #alias


''' ----------------------------- PlotInfo ----------------------------- '''

class PlotInfoPiece():
    '''info about some piece of a plot. Not intended for direct use.'''
    def has(self, attr):
        '''returns whether self has attr AND self.attr is not NO_VALUE'''
        return getattr(self, attr, NO_VALUE) is not NO_VALUE

    def get_existing(self, keys=None):
        '''return dict of {key: self.key for key in keys if self.has(key)}.
        if keys is None, use self.INFO_ATTRS.
        '''
        if keys is None: keys = self.INFO_ATTRS
        return {key: getattr(self, key) for key in keys if self.has(key)}

    def get_settable(self):
        '''return dict of settable kwargs for settings set to non-default values in self.
        The implementation here just returns self.get_existing().
        '''
        return self.get_existing()

    def with_setting(self, setting=dict(), **kw):
        '''returns a copy of self, with the given settings.
        setting can be a dict, or a PlotInfoPiece.
        if it is a PlotInfoPiece, extract settings via setting.get_settable().
        any additional kw may be provided explicitly as well;
            additional kw overwrite any kw provided in 'setting'.
        '''
        kw_use = self.get_settable()
        if isinstance(setting, PlotInfoPiece):
            setting = setting.get_settable()
        kw_use.update(setting)
        kw_use.update(kw)
        return type(self)(**kw_use)

    def __repr__(self):
        contents = [f'{key}={getattr(self, key)}' for key in self.INFO_ATTRS if self.has(key)]
        return f"{type(self).__name__}({', '.join(contents)})"

class PlotInfoAxes(PlotInfoPiece):
    '''class with "style" details for plotting x & y axes.
    This may include:
        xlabel, xlimits, xticks, xticklabels,
        ylabel, ylimits, yticks, yticklabels,
        title,

    Use self.apply() to apply this info to an existing axis object.
        self.apply(ax) applies to ax; self.apply() applies to plt.gca().
    '''
    INFO_ATTRS = (
        'xlabel', 'ylabel',
        'xlim', 'ylim',
        'xticks', 'yticks',
        'xticklabels', 'yticklabels',
        'title',
        )

    def __init__(self, *,
                    xlabel=NO_VALUE, ylabel=NO_VALUE,
                    xlim=NO_VALUE, ylim=NO_VALUE,
                    xticks=NO_VALUE, yticks=NO_VALUE,
                    xticklabels=NO_VALUE, yticklabels=NO_VALUE,
                    title=NO_VALUE,
                ):
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xlim = xlim
        self.ylim = ylim
        self.xticks = xticks
        self.yticks = yticks
        self.xticklabels = xticklabels
        self.yticklabels = yticklabels
        self.title = title

    def apply(self, ax=None):
        '''apply the PlotAxesInfo to Axes ax.
        if ax is None, use ax=plt.gca().
        '''
        ax = plt.gca() if ax is None else ax
        # apply the info.
        if self.has('xlabel'):
            ax.set_xlabel(self.xlabel)
        if self.has('ylabel'):
            ax.set_ylabel(self.ylabel)
        if self.has('xlimits'):
            ax.set_xlim(self.xlimits)
        if self.has('ylimits'):
            ax.set_ylim(self.ylimits)
        if self.has('xticks'):
            ax.set_xticks(self.xticks)
        if self.has('yticks'):
            ax.set_yticks(self.yticks)
        if self.has('xticklabels'):
            ax.set_xticklabels(self.xticklabels)
        if self.has('yticklabels'):
            ax.set_yticklabels(self.yticklabels)
        if self.has('title'):
            ax.set_title(self.title)


class PlotInfoImshow(PlotInfoPiece):
    '''class with "style" details about how to imshow something.
    This may include:
        (for imshow) extent, cmap, norm, interpolation, origin, aspect, vmin, vmax, alpha
        (for colorbar) label
        (for making colorbar axes) cax_pad, cax_size
        (for main plot axes) info_axes  (a PlotInfoAxes object), or kwargs used to create info_axes

    if any other kwargs are entered, use them to create a PlotInfoAxes instead
        (and save it to self.info_axes.)

    Use self.plot(array) to imshow the array with these settings.
    (Also plots colorbar, by default; use cbar=False to skip plotting colorbar)

    '''
    IMSHOW_ATTRS = ('extent','cmap','norm','interpolation','origin','aspect','vmin','vmax','alpha')
    COLORBAR_ATTRS = ('label', )
    CAX_ATTRS = ('cax_pad', 'cax_size')
    INFO_ATTRS = (
        *IMSHOW_ATTRS,
        *COLORBAR_ATTRS, 'cbar',
        *CAX_ATTRS,
        'info_axes',
        )


    def __init__(self, *, extent=NO_VALUE, cmap=NO_VALUE, interpolation=NO_VALUE,
                 origin=NO_VALUE, aspect=NO_VALUE, vmin=NO_VALUE, vmax=NO_VALUE, alpha=NO_VALUE,
                 cbar=True, label=NO_VALUE, cax_pad=NO_VALUE, cax_size=NO_VALUE,
                 info_axes=NO_VALUE, **kw__info_axes,
                ):
        # imshow
        self.extent = extent
        self.cmap = cmap
        self.interpolation = interpolation
        self.origin = origin
        self.aspect = aspect
        self.vmin = vmin
        self.vmax = vmax
        self.alpha = alpha
        # cbar
        self.cbar = cbar
        self.label = label
        self.cax_pad = cax_pad
        self.cax_size = cax_size
        # info_axes
        if len(kw__info_axes)>0:
            if info_axes is not NO_VALUE:
                errmsg = f"Cannot provide 'info_axes' AND these kwargs: {list(kw__info_axes.keys())}"
                raise ValueError(errmsg)
            info_axes = PlotInfoAxes(**kw__info_axes)
        self.info_axes = info_axes

    def get_settable(self):
        '''return dict of settable kwargs for settings set to non-default values in self.
        returns self.get_existing() but remove info_axes, and include info_axes.get_settable().
        '''
        kw = self.get_existing()
        kw.pop('info_axes', None)
        if self.has('info_axes'):
            kw.update(self.info_axes.get_settable())
        return kw

    def imshow(self, array, *, ax=None, **kw__imshow):
        '''imshow the array, using the settings in self as defaults.
        (If a key is provided in self & in kwargs, use the value from kwargs.)
        See also: self.colorbar, self.plot

        ax: None or matplotlib axes
            if provided, use ax.imshow instead of plt.imshow.

        returns result of plt.imshow.
        '''
        kwi = self.get_existing(self.IMSHOW_ATTRS)
        kwi.update(kw__imshow)
        if ax is None:
            result = plt.imshow(array, **kwi)
            ax = plt.gca()
        else:
            result = ax.imshow(array, **kwi)
        if self.has('info_axes'):
            self.info_axes.apply(ax)
        return result

    def colorbar(self, mappable=None, *, cax=None, ax=None, sca=False, **kw__colorbar):
        '''create the colorbar, using the settings in self as defaults.
        like plt.colorbar, except will use make_colorbar_axes if cax is None.
        See also: self.imshow, self.plot

        ax: matplotlib Axes object
            make colorbar axes next to this axis.
            if None, use plt.gca().
        sca: bool
            whether to set the colorbar axes as the current axes.

        returns result of plt.colorbar.
        '''
        ax_before = plt.gca()
        kw_cbar = self.get_existing(self.COLORBAR_ATTRS)
        kw_cbar.update(kw__colorbar)
        if cax is None:
            kw_cax = self.get_existing(self.CAX_ATTRS)
            kw_cax = {(k[len('cax_'):] if k.startswith('cax_') else k) : v for k,v in kw_cax.items()}
            cax = make_cax(ax=ax)
        cb = plt.colorbar(mappable=mappable, cax=cax, **kw_cbar)
        if not sca:  # "don't set current axes to colorbar", i.e., restore old axes.
            plt.sca(ax_before)
        return cb

    def plot(self, array, *, ax=None, cbar=None, cax=None, sca=False, **kw__imshow):
        '''imshow the array, using the settings in self as defaults.
        (If a key is provided in self & in kwargs, use the value from kwargs.)

        returns (result of plt.imshow, result of plt.colorbar)

        ax: None or matplotlib axes
            if provided, use ax.imshow instead of plt.imshow.
        cbar: None or bool
            whether to add a plt.colorbar()
            if None, use self.cbar (default True)
        cax: None or matplotlib axes
            if provided, use cax=cax during plt.colorbar()
        sca: bool
            whether to set the colorbar axes as the current axes.
        '''
        im = self.imshow(array, ax=ax, **kw__imshow)
        cb = self.colorbar(cax=cax, sca=sca)
        return (im, cb)


''' ----------------------------- Labels ----------------------------- '''

def relocate_axis_label(axis='y', loc=None, ax=None):
    '''relocate the ticks and label for the given axis to the given location.
    loc: None (for any axis), 'left' or 'right' (for axis 'y'), or 'top' or 'bottom' (for axis 'x')
        where to put the new ticks and label. strings are case-insensitive.
        None --> flip from where it is now.
                 E.g. if axis='y' and label is at 'left', move to 'right'.
        else --> move to the given location.
    ax: None or an axes object
        None --> use plt.gca()
        else --> use this axis instead of plt.gca().

    returns the new location (as a string).
    '''
    # get axis
    axis = axis.lower()
    xax = getattr(plt.gca() if ax is None else ax, '{x}axis'.format(x=axis))  # x or y axis.
    # get the loc to move to.
    if loc is None:
        prev_loc = xax.get_label_position()
        loc = {'top':'bottom', 'bottom':'top', 'left':'right', 'right':'left'}[prev_loc]
    else:
        loc = loc.lower()
    # assert loc is valid for the given axis
    if axis == 'y': assert loc in ('left', 'right'), "y axis can only deal with loc 'left' or 'right'."
    elif axis == 'x': assert loc in ('top', 'bottom'), "x axis can only deal with loc 'top' or 'bottom'."
    else: assert axis in ('x', 'y'), "invalid axis; not 'x' or 'y'. got axis={}".format(repr(axis))
    # set the location.
    getattr(xax, 'tick_{}'.format(loc))()   # move the ticks   # e.g. ax.tick_right()
    xax.set_label_position(loc)
    # return the location.
    return loc

def centered_extent1D(coords, *, L=None, ndim0_ok=False):
    '''returns extent given coords (which should be sorted from min to max, and evenly spaced).
    These are the limits such that the values will line up with pixel centers,
    instead of the left and right edges of the plot.

    if L is provided, ony the first and last values of coords are used, and L is used as the length of the array.

    np.asanyarray(coords).squeeze() must have ndim <= 1. ndim==1 required if not ndim0_ok.
    if ndim0_ok and ndim==0, return np.array([coords - 0.5, coords + 0.5]);
    '''
    coords = np.asanyarray(coords).squeeze()
    assert coords.ndim <= 1
    if ndim0_ok:
        if coords.ndim == 0:
            return np.array([coords[()] - 0.5, coords[()] + 0.5])
    else:
        assert coords.ndim != 0
    if L is None:
        L = len(coords)
    npix = L - 1  # number of pixels across
    dist = (coords[-1] - coords[0])   # full length across
    d = dist / npix  # distance between pixel centers
    # add half a pixel on each end.
    return np.array([*(coords[0] + np.array([0 - d/2, dist + d/2]))])

def centered_extent(xcoords, ycoords, *, Lx=None, Ly=None, shape=None, ndim0_ok=False):
    '''returns extent (to go to imshow), given xcoords, ycoords. Assumes origin='lower'.
    Use this method to properly align extent with middle of pixels.
    (Noticeable when imshowing few enough pixels that individual pixels are visible.)

    This method handles: "Alignment is not centered if just using min & max of coordinate arrays."
        (because e.g. in x, we want center of leftmost pixel to be min(xcoords),
        but if you use extent=(min(xcoords),...) you will get that the _left_ of leftmost pixel is that value.
        so, you need to add half a pixel on each side when determining the proper extent.)
    
    xcoords and ycoords should be arrays.
    (This method uses their first & last values, and their lengths.)
    if Lx and/or Ly are provided, use them as the lengths of coords instead.

    np.asanyarray(coords).squeeze() must have ndim <= 1. ndim==1 required if not ndim0_ok.
    if ndim0_ok and ndim==0, use np.array([coords - 0.5, coords + 0.5]) for those coords.

    returns extent == np.array([left, right, bottom, top]).
    '''
    return np.array([*centered_extent1D(xcoords, L=Lx, ndim0_ok=ndim0_ok),
                     *centered_extent1D(ycoords, L=Ly, ndim0_ok=ndim0_ok)])

#aliases
extent = centered_extent
extent1D = centered_extent1D


''' ----------------------------- Plots - Colors ----------------------------- '''

def colors_from_cmap(cmap, N):
    '''get N evenly spaced colors from the colormap cmap. if cmap is a string, gets matplotlib's default.'''
    if isinstance(cmap, str):
        from matplotlib import cm
        cmap_actual = getattr(cm, cmap)
        return colors_from_cmap(cmap_actual, N)
    return cmap(np.linspace(0, 1, N))

def _colorbar_extent(under=None, over=None):
    '''returns appropriate value for 'extend' in colorbar(extend=..., ...)

    (under provided, over provided) --> value
        True,       True            --> 'both'
        True,       False           --> 'min'
        False,      True            --> 'max'
        False,      False           --> 'neither'
    '''
    lookup = {(True, True): 'both', (True, False): 'min', (False, True): 'max', (False, False): 'neither'}
    return lookup[(under is not None, over is not None)]

def _set_colorbar_extend(cmap):
    '''sets cmap.colorbar_extend appropriately.
    Destructive; cmap will be altered directly (as opposed to returning a new cmap).

    compares cmap(0.0) to cmap.get_under(), and cmap(1.0) to cmap.get_over().
        If unequal, makes triangle at that end.
    E.g. if cmap(0.0) != cmap.get_under(), but cmap(1.0) == cmap.get_over(),
        makes triangle at bottom but not top. extend='min'.

    returns cmap.
    '''
    under_was_set = True if ( not np.array_equal(cmap(0.0), cmap.get_under()) ) else None
    over_was_set  = True if ( not np.array_equal(cmap(1.0), cmap.get_over())  ) else None
    cmap.colorbar_extend = _colorbar_extent(under=under_was_set, over=over_was_set)

def with_colorbar_extend(cmap):
    '''returns a copy of cmap with colorbar_extend set appropriately, based on cmap's extremes.'''
    cmap_c = cmap.copy()
    _set_colorbar_extend(cmap_c)
    return cmap_c

def extended_cmap(cmap=None, under=None, over=None, bad=None, N=None):
    '''creates a cmap with the extremes provided.

    cmap: None, string, or colormap
        None --> use the matplotlib default colormap from rc params.
        string --> getattr(matplotlib.cm, cmap)
            E.g. 'viridis' --> matplotlib.cm.viridis.
        colormap --> use the colormap provided.
    under, over, bad: None, string, RGB, RGBA, or any other color which matplotlib can understand.
        under: color for points less than vmin.
            A triangle with this color will appear at bottom of colorbar, if under is set.
        over: color for points greater than vmax.
            A triangle with this color will appear at top of colorbar, if over is set.
        bad: color for bad points (NaNs).
            There is no representation of this color on the colorbar;
            be sure to tell viewers the meaning of this color if it appears on the plot.
    N: None or int
        resample cmap to this many points, if cmap is provided via string.
        (If cmap is a colormap object, ignore this kwarg.)

    returns: the resulting colormap.
    '''
    cmap0 = plt.get_cmap(cmap, N)
    cmap1 = cmap0.with_extremes(under=under, over=over, bad=bad)
    cmap2 = with_colorbar_extend(cmap1)
    return cmap2

cmap_extended = extended_cmap  # alias

def make_colorbar_axes(location='right', ticks_position=None, ax=None, pad=0.01, size=0.02):
    ''' Creates an axis appropriate for putting a colorbar.

    location: 'right' (default), 'left', 'top', or 'bottom'
        location of colorbar relative to image.
        Note: you will want to set orientation appropriately.
    ticks_position: None (default), 'right', 'left', 'top', or 'bottom'
        None -> ticks are on opposite side of colorbar from image.
        string -> use this value to set ticks position.
        NOTE: for horizontal colorbars, also use plt.colorbar(..., orientation='horizontal').
        NOTE: for horizontal colorbars, the ticks_position may be overriden by the plt.colorbar() call.
    ax: None or axes object
        None -> use plt.gca()
        this is the axes which will inform the size and position for cax.
        it is appropriate to use ax = axes for the image.
    pad: number (default 0.01)
        padding between cax and ax.
        TODO: what does the number really mean?
    size: number (default 0.02)
        size of colorbar.
        TODO: what does the number really mean?

    Adapted from https://stackoverflow.com/a/56900830.
    Returns cax.
    '''
    if ax is None:
        ax = plt.gca()
    p = ax.get_position()
    # calculate cax params.
    ## fig.add_axes(rect) has rect=[x, y, w, h],
    ## where x and y are location for lower left corner of axes.
    ## and w and h are width and height, respectively.
    assert location in ('right', 'left', 'top', 'bottom')
    if location in ('right', 'left'):
        y = p.y0
        h = p.height
        w = size
        if location == 'right':
            x = p.x1 + pad
        else: #'left'
            x = p.x0 - pad - w
    else: #'top' or 'bottom'
        x = p.x0
        w = p.width
        h = size
        if location == 'top':
            y = p.y1 + pad
        else: #'bottom'
            y = p.y0 - pad - h

    # make the axes
    cax = plt.gcf().add_axes([x, y, w, h])
    
    # Change ticks position
    if ticks_position is None:
        ticks_position = location
    if ticks_position in ('left', 'right'):
        cax.yaxis.set_ticks_position(ticks_position)
    else: #'top' or 'bottom'
        cax.xaxis.set_ticks_position(ticks_position)

    return cax

make_cax = make_colorbar_axis = make_colorbar_axes  # alias


''' --------------------------- imshow symlog --------------------------- '''

CENTERED_LIMIT_MODE = 'larger'   # default

def centered_limits(data, center=None, limit_mode=CENTERED_LIMIT_MODE, _return_center=False):
    '''returns limits for data cenetered on the mean (or center, if provided).
    extends to the furthest away from center in either direction.
    E.g. data=[0, 2, 7] will have limits (-1, 7) since the mean is 3, 7-3==4, and 3-4==-1.

    limit_mode: 'larger', 'smaller', 'upper', or 'lower'. Default 'larger'.
        'larger' : limits based on max(|vmax-center|, |center-vmin|).
        'smaller': limits based on min(|vmax-center|, |center-vmin|).
        'upper'  : limits based on vmax-center
        'lower'  : limits based on center-vmin.
        None --> use default value ('larger')
    
    returns (lower limit, upper limit).
    if _return_center, instead returns (lower, center, upper)
    '''
    data = np.asanyarray(data)
    top = data.max()
    mid = data.mean() if center is None else center
    bot = data.min()
    diff_lower = mid - bot
    diff_upper = top - mid
    # choose which diff to use for limit.
    if limit_mode is None:
        limit_mode = CENTERED_LIMIT_MODE  # None --> default
    if limit_mode == 'larger':     # calculate whether to use upper or lower.
        limit_mode = 'upper' if diff_upper >= diff_lower else 'lower'
    elif limit_mode == 'smaller':  # calculate whether to use upper or lower.
        limit_mode = 'upper' if diff_upper <= diff_lower else 'lower'
    # calculate the limits.
    if limit_mode == 'upper':  # use upper
        lim_lower = mid - diff_upper
        lim_upper = top
    elif limit_mode == 'lower':
        lim_lower = bot
        lim_upper = mid + diff_lower
    else:
        raise ValueError(f"unsupported limit mode: {repr(limit_mode)}")
    # return the result.
    if _return_center:
        return (lim_lower, mid, lim_upper)
    else:
        return (lim_lower, lim_upper)

def centered_ticks(data, N=10, center=None):
    '''ticks centered at center (or data.mean()) and symmetric,
    extending up to max(top - mid, mid - bot) away from mid in both directions
    (where bot=data.min(), mid=data.mean(), top=data.max())
    '''
    lower, center, upper = centered_limits(data, center=center, _return_center=True)
    diff = center - lower
    if N%2 == 0:
        N += 1   # odd N ensures center appears in the result.
    return np.linspace(-diff, diff, N) + center

def symlog_ticks(data, N=10, linthresh=1, center=None):
    '''centered_ticks but using even logarithmic spacing instead of linear spacing.
    linear for values between center-linthresh and center+linthresh.
    '''
    lower, center, upper = centered_limits(data, center=center, _return_center=True)
    diff = center - lower
    assert linthresh < diff, "linthresh is supposed to be smaller than vmax - center."
    n = N // 2
    space = np.exp(np.linspace(np.log(linthresh), np.log(diff), n))
    return np.concatenate((-space[::-1], [0], space)) + center

def imshow_symlog(arr, vmin=None, vmax=None, logthresh=None, logstep=None, linscale=1,
                  logthresh_margin=None, minticks=5, maxticks=13,
                  make_cbar=True, kw_cbar=dict(), **kwargs):
    '''make imshow with a symmetric log scale.
    arr: 2D array
        imshow of this array
    vmin, vmax: None or value
        vmin and vmax of plot.
        None --> use arr.min(), arr.max().
    logthresh: None or value
        Threshold for exponents, below which the scale is linear.
        E.g. logthresh = 2 --> linear between -100 and 100.
        None --> floor(min(log10(|arr|)))
            if logthresh_margin is provided, add (before floor):
            logthresh_margin * (max(log10(|arr|)) - min(log10(|arr|))).
    logstep: None or value
        step between ticks' exponents.
        E.g. logstep=2 --> exponents increase by 2, e.g. 10^2, 10^4, 10^6.
        None --> use an integer such that there are at least minticks and at most maxticks ticks on the plot.
    linscale: value, default 1.
        ratio of the entire linear section of the scale to the space between two ticks.
        E.g. linscale=2 --> linear section is twice as large as the space between two ticks.
    make_cbar: bool, default True
        whether to make colorbar.
        (Useful when making multiple plots, e.g. subplots, which share a color scheme.)
    kw_cbar: dict
        **kw_cbar is passed to plt.colorbar.
    **kwargs:
        **kwargs are passed to plt.imshow.

    returns (image, cbar).  If make_cbar is False, just return image.
    '''
    # Adapted from https://stackoverflow.com/q/39256529
    arr = np.asanyarray(arr)
    
    # handle "no negative values" case - make a log plot.
    if finite_min(arr) >= 0:
        img = plt.imshow(arr, norm=colors.LogNorm(vmin=vmin, vmax=vmax), **kwargs)
        ax=plt.gca()
        cb  = plt.colorbar(**kw_cbar)
        plt.sca(ax)
        return img, cb

    # else, make a symlog plot:
    vmin, vmax, logthresh, logstep, linscale = symlog_defaults(arr,
                                                               vmin, vmax, logthresh, logstep, linscale,
                                                               logthresh_margin, minticks, maxticks)
    max_pos_log=int(np.ceil(np.log10(vmax)))    # e.g. (10^-2, 10^5, 10^-4, 10^7) --> 7
    max_neg_log=int(np.ceil(np.log10(-vmin)))   # e.g. (10^-2, 10^5, 10^-4, 10^7) --> -4
    tick_locations=([-(10.**x) for x in np.arange(logthresh, max_neg_log+1, logstep)][::-1]
                    +[0.0]
                    +[(10.**x) for x in np.arange(logthresh, max_pos_log+1, logstep)] )
    
    
    img=plt.imshow(arr,
                   norm=colors.SymLogNorm(10**logthresh, linscale=linscale/2, vmin=vmin, vmax=vmax),
                   **kwargs)
    if make_cbar:
        ax=plt.gca()
        if 'cax' not in kw_cbar:
            kw_cbar = {**kw_cbar, 'cax': make_cax()}   # new kw_cbar. Original is part of function definition.
        cb=plt.colorbar(ticks=tick_locations, format=ticker.LogFormatterMathtext(), **kw_cbar)
        plt.sca(ax)
        return img,cb
    else:
        return img

def symlog_defaults(arr, vmin=None, vmax=None, logthresh=None, logstep=None, linscale=1,
                   logthresh_margin=None, minticks=5, maxticks=13):
    '''calculates defaults of vmin, vmax, linthresh, logstep, linscale.
    
    if not None, use the value provided. Else, use these values:
    vmin --> arr.min(), ignoring nans and infs
    vmax --> arr.max(), ignoring nans and infs
    logthresh --> floor( min(log10(|arr|)) + logthresh_margin * (max(log10(|arr|)) - min(log10(|arr|))) )
    logstep --> use an integer such that there are at least minticks and at most maxticks ticks on the plot.
        if that is not possible, break the maxticks rule (but strictly require the minticks rule).
        Tries to have as many ticks (up to maxticks) as possible (with logstep >= 1)
    linscale --> (doesn't have a default if None; must be entered as a value. default 1)
    '''
    if vmin is None: vmin = finite_min(arr)
    if vmax is None: vmax = finite_max(arr)
    
    log_arr = np.log10(np.abs(arr))
    min_log = finite_min(log_arr)
    if logthresh is None:
        logthresh = min_log
        if logthresh_margin is not None:
            logthresh = logthresh + logthresh_margin * (finite_max(log_arr) - min_log)
        logthresh = np.floor(logthresh)
    
    if logstep is None:
        assert minticks >= 5, f"minticks must be at least 5, but got {minticks}."
        assert maxticks > minticks, f"need maxticks ({maxticks}) larger than minticks ({minticks})"
        max_pos_log=int(np.ceil(np.log10(vmax)))    # e.g. (10^-2, 10^5, 10^-4, 10^7) --> 7
        max_neg_log=int(np.ceil(np.log10(-vmin)))   # e.g. (10^-2, 10^5, 10^-4, 10^7) --> -4
        n_decades = (max_neg_log - logthresh) + (max_pos_log - logthresh)   # number of adjustable decades.
        # < note these 3 ticks must always be present: -logthresh, 0, +logthresh.
        logstep = max(1, np.ceil(n_decades / (maxticks - 3)))
        
    return vmin, vmax, logthresh, logstep, linscale

def invert_limits(ax):
    '''invert the limits for ax. E.g. -5 to 7 becomes 7 to -5.'''
    return ax.set_ylim((lambda ylim: (ylim[1], ylim[0]))(ax.get_ylim()))

# tricks not included in functions:
# turn off tick labels:
#   plt.gca().yaxis.set_ticklabels([])


''' --------------------------- Tick Formatters & Locators --------------------------- '''

def TickFormatterPrettySciNotation(fmt='{:.2f}', base=10, force_exponent=False):
    '''returns a tick formatter which makes pretty scientific notation.
    ticks will look like coeff x base^{exp}.
    Pretty features:
    - the coeffs will be formatted via fmt
    - force_exponent controls behavior when exp=0.
        if force_exponent, always include the exponent. Else, for exp=0 only include coeff.

    [TODO] compatibility with fmt string like {: .3f}, which should
        put a space in front of positive values so the decimal lines up with negative values.
        The challenge here is how latex handles spacing.

    Example use:
        import BUpytools as bup
        plt.imshow(data)
        cax = bup.plots.make_cax()
        plt.colorbar(cax=cax)
        cax.yaxis.set_major_formatter(bup.plots.TickFormatterPrettySciNotation())
    '''
    _tick_strfunc = _tick_strfunc_PrettySciNotation(fmt=fmt, base=base, force_exponent=force_exponent)
    return ticker.FuncFormatter(_tick_strfunc)

def _tick_strfunc_PrettySciNotation(fmt='{:.2f}', base=10, force_exponent=False):
    '''returns a function which converts values to strings for labeling matplotlib ticks.
    Uses scientific notation.
    '''
    def val_to_mpl_scifmt(val, pos=None):
        '''convert val to string in scientific notation appropriate for plotting.
        the pos kwarg ensures this func is compatible with mpl.ticker.FuncFormatter.
        '''
        scifmt = sci_notation(val, fmt=fmt, base=base, force_exponent=force_exponent)
        return fr'$\mathdefault{{{scifmt}}}$'
    return val_to_mpl_scifmt


class FormatScalarFormatter(ticker.ScalarFormatter):
    '''formats the mantissa values on the axis (without affecting the offset or exponent).
    adapted from https://stackoverflow.com/a/45816969
    '''
    def __init__(self, fformat="%.1f", useOffset=True, useMathText=True):
        self.fformat = fformat
        ticker.ScalarFormatter.__init__(self, useOffset=useOffset,
                                             useMathText=useMathText)

    def _set_format(self, vmin=None, vmax=None):
        self.format = self.fformat
        self.format = fr'$\mathdefault{{{self.format}}}$'

    def __call__(self, x, pos=None):
        # '+0.0' looks bad, so we remove the sign from (sub)strings representing the number 0.
        # but otherwise we are just doing super().__call__.
        result = super().__call__(x, pos=pos)
        result = _remove_signs_from_0s(result)
        return result


class FixedSymCenteredLocator(ticker.Locator):
    '''tick locations that are fixed in symlog space around the center.
    
    all ticks are located at center + start * base**n for some integer n.
    
    n_per_side: number, default np.e.
        number of ticks to show on each side of the center.
    limit_mode: 'larger', 'smaller', 'upper', or 'lower'.
        'larger' : limits based on max(|vmax-center|, |center-vmin|).
        'smaller': limits based on min(|vmax-center|, |center-vmin|).
        'upper'  : limits based on vmax-center
        'lower'  : limits based on center-vmin.
        None --> use the default value ('larger')

    example of using with imshow:
        plt.imshow(data)
        cax = QOL.make_cax()
        cbar = plt.colorbar(cax=cax, **kw__colorbar)
        locator = QOL.FixedSymCenteredLocator(center=data.mean())
        cax.yaxis.set_major_locator(locator)
    '''
    def __init__(self, base=np.e, center=0, start=1, n_per_side=2, limit_mode=CENTERED_LIMIT_MODE):
        self.base       = base
        self.center     = center
        self.start      = start
        self.n_per_side = n_per_side
        self.limit_mode = limit_mode
        
        self.D = np.log10(base)
        
    def __call__(self):
        vmin, vmax = self.axis.get_view_interval()
        return self.tick_values(vmin, vmax)
                                    
    def tick_values(self, vmin, vmax):
        '''return tick values for the selected vmin, vmax.'''
        # convert vmin and vmax to limits:
        limits = centered_limits((vmin, vmax), center=self.center, limit_mode=self.limit_mode)
        diff   = limits[1] - self.center   # == self.center - limits[0]
        nmax  = np.floor(np.log10(diff / self.start) / self.D).astype(int)   # e.g. 7
        nvals = nmax - np.arange(self.n_per_side)[::-1]         # e.g. [6,7]
        upper = self.start * self.base ** nvals            # upper values (distance from center)
        lower = -upper[::-1]                               # lower values (distance from center)
        ticks = np.concatenate((lower, [0], upper)) + self.center
        return ticks


''' --------------------------- bbox manipulations --------------------------- '''

def bbox_add(bbox, left=0, bottom=0, right=0, top=0):
    '''return bbox after adding the values provided.
    Positive values cause growth; negative values cause shrinkage.

    Bboxes are stored by matplotlib in the form [[x0, y0], [x1, y1]]
    '''
    ((x0, y0), (x1, y1)) = bbox.get_points()
    return mpl.transforms.Bbox([[x0-left, y0-bottom], [x1+right, y1+top]])

def bbox_expand(bbox, left=0, bottom=0, right=0, top=0):
    '''return bbox after expanding by the values provided.
    Values indicate fraction of the full dimension to be added.
    Positive values cause growth; negative values cause shrinkage.
    E.g. right=-0.05 means "subtract 0.05 * the bbox width from the right side of the bbox"

    Bboxes are stored by matplotlib in the form [[x0, y0], [x1, y1]]
    '''
    ((x0, y0), (x1, y1)) = bbox.get_points()
    width, height = bbox.size
    return mpl.transforms.Bbox([[x0-left*width, y0-bottom*height], [x1+right*width, y1+top*height]])

def tight_bbox_inches(fig=None):
    '''get 'tight bbox' of fig in inches. use current figure if fig is None.'''
    fig = plt.gcf() if fig is None else fig
    return fig.get_tightbbox(fig.canvas.get_renderer())

def tight_bbox_inches_add(left=0, bottom=0, right=0, top=0, fig=None):
    '''return tightbbox of current figure, in inches, after adding the values provided.
    Positive values cause growth; negative values cause shrinkage.

    Bboxes are stored by matplotlib in the form [[x0, y0], [x1, y1]]
    '''
    return bbox_add(tight_bbox_inches(fig), left, bottom, right, top)

def tight_bbox_inches_expand(left=0, bottom=0, right=0, top=0, fig=None):
    '''return tightbbox of current figure, in inches, after expanding by the values provided.
    Values indicate fraction of the full dimension to be added.
    Positive values cause growth; negative values cause shrinkage.
    E.g. right=-0.05 means "subtract 0.05 * the bbox width from the right side of the bbox"

    Bboxes are stored by matplotlib in the form [[x0, y0], [x1, y1]]
    '''
    return bbox_expand(tight_bbox_inches(fig), left, bottom, right, top)

def tight_bbox_inches_grow(add=[0,0,0,0], expand=[0,0,0,0], fig=None):
    '''return tightbbox of current figure, in inches, after growing by the values provided.
    Values are passed to tight_bbox_inches_add or tight_bbox_inches_expand, appropriately.
    add: list of 4 values, default [0,0,0,0]
        left, bottom, right, top
        number, in inches, to add to this dimension. 
    expand: list of 4 values, default [0,0,0,0]
        left, bottom, right, top
        number, as a fraction of the present size, to add to this dimension
        e.g. [0, 0, 0.05, 0] means "add 0.05 * the bbox width to the right side of the bbox"
    (+ for grow; - for shrink)

    expands first, then adds. (Following standard "multiply before adding" order of operations)

    Bboxes are stored by matplotlib in the form [[x0, y0], [x1, y1]]
    '''
    bbox = tight_bbox_inches(fig)
    bbox = bbox_expand(bbox, *expand)
    bbox = bbox_add(bbox, *add)
    return bbox


''' --------------------------- movies / stackplots --------------------------- '''

def movie_save(ani, filename, *, verbose=True, **kw_save):
    '''save animation ani to filename.
    Intended as a helper method for other functions, e.g. movie3D and movie3D_stack.

    if filename doesn't contain '.', append '.mp4'.
    if verbose, print some helpful messages.

    returns os.path.abspath of the saved file.
    '''
    if '.' not in filename: filename = f'{filename}.mp4'
    updater = ProgressUpdatePrinter(print_freq=1 if verbose else -1)
    if 'progress_callback' not in kw_save:
        kw_save['progress_callback'] = lambda i, n: updater.print(f'saving frame {i+1} of {n}')

    ani.save(filename=filename, **kw_save)

    updater.finalize(f'movie at {os.path.abspath(filename)!r}', always=verbose)
    return os.path.abspath(filename)

def movie3D(array, filename=None, *, fps=30, dpi=None, style=None, right=None,
            kw_figure=dict(), kw_save=dict(), times=None, blit=True, verbose=True,
            preview=False):
    '''save a movie3D of array to filename.

    array: 3D array
        array to plot. Scans along the first axis of the array; imshow each frame.
        vmin & vmax of colorbar will be min & max of this array, unless provided via style.

    filename: None or string
        None --> do not save.
        string --> save to this filename. Should probably end in '.mp4'.
    fps: number
        frames per second
    style: None, dict, or PlotInfoImshow
        use this to plot the first frame;
        subsequent frames will update the plot data but will not change the style.

    right: None or number
        if provided, tells the right edge of the main axes; 1.0 means lined up with right side of figure.
        if colorbar label is getting cut off on right side of figure, use a smaller value for right.
        Default right is plt.rcParams['figure.subplot.right'].

    times: None or list of strings
        if provided, update title of plot to times[frame] at each frame.
        otherwise, use f'i={frame}'.

    preview: bool, int, or iterable.
        if False, ignore.
        if iterable, make and save movie but include only these frames (and also frame 0).
        if True or int, return (im, cb) from a single frame, and do not make the movie.
            use frame 0 if True; else, use frame int(preview).
        Useful to get formatting right without needing to wait for the full movie to render.
    '''
    frames = len(array)
    interval = 1000 / fps
    try:
        iter(preview)
    except TypeError:
        pass
    else:  # preview is iterable; make a movie of only these frames.
        frames = preview
        preview = False  # don't just return the frame0 plot.

    fig = plt.figure(dpi=dpi, **kw_figure)
    plt.subplots_adjust(right=right)
    if style is None:
        style = dict()
    if not isinstance(style, PlotInfoImshow):
        style = PlotInfoImshow(**style)
    vmin = style.vmin if style.has('vmin') else finite_min(array)
    vmax = style.vmax if style.has('vmax') else finite_max(array)
    i0 = 0 if (preview is True or preview is False) else int(preview)
    im, cb = style.plot(array[i0], vmin=vmin, vmax=vmax)
    plt.title(f'i={i0}' if times is None else times[i0])

    if not (preview is False):
        return (im, cb)

    def animate(frame):
        plt.title(f'i={frame}' if times is None else times[frame])
        im.set_data(array[frame])
        return im,

    ani = FuncAnimation(fig, animate, frames=frames, interval=interval,
                        blit=blit)  # much slower if blit=False.
    if filename is not None:
        movie_save(ani, filename, verbose=verbose, **kw_save)
    return ani

def stackplot(arrays, *, lims_from=None, styles=None, global_style=None,
              apply=None, keys=None,
              plotsize=None, hspace=None, right=None, top=None, bottom=None, left=None, **kw__subplots):
    '''create a single stackplot of these arrays, with these styles.
    arrays: dict of 2D arrays
        arrays to plot.
    lims_from: None or dict
        {key matching any key in arrays: [values containing vmin & vmax] to use for that array}
        if provided, use this to determine vmin & vmax for arrays, when necessary.
        (only utilized when style doesn't indicate vmin and/or vmax)
    styles: None or dict
        {key matching any key in arrays: style to use when plotting that array}
        each "style" is a PlotInfoImshow object or a dict of kwargs for PlotInfoImshow.
    global_style: None, dict, or PlotInfoImshow
        if provided, apply this style to all plots (unless overridden by individual styles).
        E.g. global_style=dict(aspect='auto') applies that aspect to all plots,
            but if styles=dict(..., arr3=dict(..., aspect='equal')) then use aspect='equal' for arr3.
    apply: None or callable
        if provided, apply(array) for each array, before plotting.
    keys: None or iterable of strings
        if provided, use this instead of arrays.keys(). tells order & which arrays to plot.
    plotsize: None or tuple
        if provided, tells (xsize, ysize) of each subplot, in inches.
        if figsize also provided, will ignore plotsize.
    hspace: None or number
        hspace for subplots_adjust
    right: None or number
        if provided, tells the right edge of the main axes; 1.0 means lined up with right side of figure.
        if colorbar label is getting cut off on right side of figure, use a smaller value for right.
        Default right is plt.rcParams['figure.subplot.right'].
    top, left, bottom: None or number
        similar to right, but for top, left, bottom sides of main axis instead.
        for left and bottom, 0.0 means lined up with left/bottom side of figure.

    returns dict(fig=figure, axes=axes, ims=imshow results, cbs=colorbars)
    '''
    if keys is None: keys = tuple(arrays.keys())
    if apply is not None: arrays = {key: apply(arrays[key]) for key in keys}
    N = len(keys)
    if (plotsize is not None) and ('figsize' not in kw__subplots):
        kw__subplots['figsize'] = (plotsize[0], N * plotsize[1])
    fig, axes = plt.subplots(nrows=N, **kw__subplots)
    plt.subplots_adjust(hspace=hspace, right=right, top=top, bottom=bottom, left=left)

    if styles is None: 
        styles = dict()
    else:
        styles = styles.copy()
    for key in keys:
        if key in styles:
            style = styles[key]
            if not isinstance(style, PlotInfoImshow):
                style = PlotInfoImshow(**style)
        else:
            style = PlotInfoImshow()
        styles[key] = style
    for i, key in enumerate(keys):
        style = styles[key]
        if not style.has('label'):
            style = style.with_setting(label=key)
        if (i < N-1) and (not style.has('info_axes') or not style.info_axes.has('xticklabels')):
            style = style.with_setting(xticklabels=[])
        styles[key] = style

    if global_style is not None:
        if not isinstance(global_style, PlotInfoImshow):
            global_style = PlotInfoImshow(**global_style)
        for key, style in styles.items():
            styles[key] = global_style.with_setting(style)

    ims = dict()
    cbs = dict()
    for ax, key in zip(axes, keys):
        # pre-processing of array & defaults for plot
        arr = arrays[key]
        style = styles[key]
        if (lims_from is None) or (key not in lims_from):
            limvals = arr  # use arr to determine lims (if not indicated by style)
        else:
            limvals = lims_from[key]  # lims are somewhere in these vals, maybe not in order.
        vmin = style.vmin if style.has('vmin') else finite_min(limvals)
        vmax = style.vmax if style.has('vmax') else finite_max(limvals)
        # actually plot it
        plt.sca(ax)
        ims[key], cbs[key] = style.plot(arr, vmin=vmin, vmax=vmax)

    return dict(fig=fig, axes=axes, ims=ims, cbs=cbs)

def movie3D_stack(arrays, filename=None, *, fps=30, styles=None, keys=None, apply=None, times=None,
                  hspace=None, verbose=True, blit=True, kw_save=dict(), preview=False, **kw__stackplot):
    '''save a movie of a stackplot, to filename.

    arrays: dict of 3D arrays
        arrays to plot. Scans along the first axis of each array; imshow each frame.
        All arrays should have the same length in axis 0.
    filename: string
        where to save the movie. If it doesn't have a '.' in it, will append '.mp4'.
    styles: None or dict
        {key matching any key in arrays: style to use when plotting that array}
        each "style" is a PlotInfoImshow object or a dict of kwargs for PlotInfoImshow.
    keys: None or iterable of strings
        if provided, use this instead of arrays.keys(). tells order & which arrays to plot.
    apply: None or callable
        if provided, apply(array) for each array, before plotting.
    times: None or list of strings
        if provided, update title of plot to times[frame] at each frame.
        otherwise, use f'i={frame}'.
    hspace: None or number
        hspace for subplots_adjust

    preview: bool, int, or iterable.
        if False, ignore.
        if iterable, make and save movie but include only these frames (and also frame 0).
        if True or int, return stackplot of a single frame, and do not make the movie.
            use frame 0 if True; else, use frame int(preview).
        Useful to get formatting right without needing to wait for the full movie to render.

    TROUBLESHOOTING TIPS:
        - if right-side of movie is getting cut off, try using right=0.8 (or smaller).
    '''
    if keys is None: keys = tuple(arrays.keys())
    array0 = arrays[keys[0]]
    frames = len(array0)
    interval = 1000 / fps

    if apply is not None: arrays = {key: apply(arrays[key]) for key in keys}

    try:
        iter(preview)
    except TypeError:
        pass
    else: # preview is iterable; so make a movie but with only these frames.
        frames = preview
        preview = False  # don't just return the frame0 plot.
    # frame 0
    i0 = 0 if (preview is True or preview is False) else int(preview)
    arrays_0 = {key: arrays[key][i0] for key in keys}
    frame0 = stackplot(arrays_0, styles=styles, lims_from=arrays,
                       keys=keys, hspace=hspace, apply=None, **kw__stackplot)
    frame0['axes'][0].set_title(f'i={i0}' if times is None else times[i0])
    if not (preview is False):
        return frame0

    ims = frame0['ims']

    # other frames
    def animate(frame):
        frame0['axes'][0].set_title(f'i={frame}' if times is None else times[frame])
        for key, im in ims.items():
            im.set_data(arrays[key][frame])
        return list(ims.values())

    ani = FuncAnimation(frame0['fig'], animate, frames=frames, interval=interval,
                        blit=blit)  # much slower if blit=False.

    if filename is not None:
        movie_save(ani, filename, verbose=verbose, **kw_save)

    return ani
