"""
Convenience for strings.
"""

# import built-in modules
import collections
import re

# import stuff from internal modules
from .api import (
    is_integer
)
from .imports import (
    ImportFailed,
)

# import external public modules
try:  # NUMPY
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

# import internal stuff into this namespace for convenience (but not used in this module)
from .api import (
    print_clear,
)


def strlist(x, brackets='[]', sep=', '):
    '''convert list into string list.
    e.g. ['hi', '1.5e7', 73, 'text'] --> '[hi, 1.5e7, 73, text]'
    '''
    return brackets[0] + sep.join([str(s) for s in x]) + brackets[1]

def sci_notation(val, fmt='{: .3f}', base=10.0, force_exponent=False):
    '''string for val in scientific notation, using the base provided.
    coefficient will be hit by fmt (default '{: .3f}).
    
    force_exponent: bool, default False
        behavior when exponent is 0.
        False --> do not include exponent if exponent is 0.
        True --> include exponent even if it is 0.
        
    Examples:
        sci_notation(7654321)  -->  ' 7.654 \times 10^{6}'
        sci_notation(7.65)     -->  ' 7.650'
        sci_notation(-7e-6)     -->  '-7.000 \times 10^{-6}'
        
    returns string for scientific notation of val.
    '''
    # calculations
    sign = 1 if (val > 0) else -1
    val  = abs(val)
    if val == 0:
        coeff = exponent = 0
    else:
        logval   = np.log10(val) / np.log10(base)      # log of val using this base.
        exponent = np.floor(logval).astype(int)        # integer exponent
        coeff    = sign * base ** (logval - exponent)  # coefficient
    # strings
    str_coeff = fmt.format(coeff)
    if (exponent == 0) and (not force_exponent):
        result = str_coeff
    else:
        str_base  = fmt_probable_int(base, fmt_if_float='{:.2f}')
        str_exp   = str(exponent)
        result = fr'{str_coeff} \times {str_base}^{{{str_exp}}}'
    return result 

def format_complex(val, fmt='{:.3e}'):
    '''format a number as if it is a complex value.'''
    real, imag = val.real, val.imag
    realstr = (f' {fmt}'.format(real)) if real > 0 else (f'-{fmt}'.format(-1 * real))
    imagstr = (f'+ {fmt} j'.format(imag)) if imag > 0 else (f'- {fmt} j'.format(-1 * imag))
    return f'{realstr} {imagstr}'

fmtc = format_complex   # alias

def fmt_probable_int(val, fmt_if_float='{:.2f}'):
    '''format val. if val is an integer, use str(val).
    if val is not an integer, use fmt_if_float.
    '''
    if is_integer(val):
        result = str(int(val))
    else:
        result = fmt_if_float.format(val)
    return result

def _escape_re_metachars(s):
    '''escapes all the re metachars except \, {, and }.'''
    for char in '.^$*+?[]|()':
        s = s.replace(char, '\\' + char)
    return s

def fmt_dict_vals(d, fmt='{:.3e}', recursive=True):
    '''return dict with values converted to string, using fmt string.
    recursive: True, False, or int.
        True --> when d[key] is a dict,  convert d[key] via fmt_dict_vals
        False --> when d[key] is a dict, convert d[key] via fmt.format(d[key])
        int --> like True, but uses fmt_dict_vals(d[key], recursive - 1).
        dict-ness is checked via isinstance(x, collections.abc.Mapping)
    '''
    r = dict()
    for key, val in d.items():
        if recursive>0 and isinstance(val, collections.abc.Mapping):
            next_recursive = (True if recursive is True else recursive - 1)
            r[key] = fmt_dict_vals(val, fmt=fmt, recursive=next_recursive)
        else:
            r[key] = fmt.format(val)
    return r

def _remove_sign_from_0(s):
    '''replace first instance of +0 or -0 in s with 0.
    Checks for the apparent numerical value, not just the first digit.
    E.g. '+0.05' --> '+0.05'. But '+0.000 --> 0.000'.
    returns (result, whether any signs were removed, index of first unchecked character)
    '''
    result, removed_any, i_unchecked = s, False, len(s)
    pattern = re.compile(r'[+-]\s*0[.]?0*')
    matched = re.search(pattern, s)
    if matched:
        start, end = matched.span()   # indices for start and end of match
        next_char = '' if end >= len(s) else s[end]
        if next_char in '123456789':
            result_end, removed_any, i_unchecked = _remove_sign_from_0(s[end:])
            result = s[:end] + result_end
        else:
            result = s[:start] + s[start:end].replace('+', '').replace('-', '') + s[end:]
            removed_any = True
            i_unchecked = end
    return (result, removed_any, i_unchecked)

def _remove_signs_from_0s(s):
    '''replace all instances of +0 or -0 in s with 0.
    Checks for the apparent numerical value, not just the first digit.
    E.g. '+0.05' --> '+0.05'. But '+0.000 --> 0.000'.
    returns the adjusted string.
    '''
    result, removed_any, i_unchecked = _remove_sign_from_0(s)
    if removed_any:
        result = result[:i_unchecked]
        result += _remove_signs_from_0s(s[i_unchecked:])
    return result