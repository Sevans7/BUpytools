"""
Picking unit prefixes. (Helps for making nice plots!)
"""

# import built-in modules
import collections

# import stuff from internal modules
from .imports import (
    ImportFailed,
)

# import external public modules
try:  # NUMPY
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

# defaults
UNIT_PREFIXES = (
                 (1e9, 'G'), (1e6, 'M'),  (1e3, 'k'),
                 (1, ''),
                 (1e-3, 'm'), (1e-6, '$\\mu$'), (1e-9, 'n')
                )

def pick_units(value, return_value=True, more_units=[], units=UNIT_PREFIXES, _weight=0.1):
    '''picks decent units for value.
    returns (divide_by, prefix), as a namedtuple.
    divide value by divide_by to get value in <prefix> units.
    
    namedtuple help:
        You can access contents via result[i] or result.key.
        result.divide_by == result[0]
        result.prefix    == result[1]

    PARAMETERS
    ----------
    value: scalar or array.
        scalar -> pick largest provided unit less than value, or 1 if value is 0.
        array  -> pick_units of (1 - _weight) * min(|value|) + _weight * max(|value|)
                    removes 0's before finding min & max. default _weight is 0.05.
    return_val: bool, Default: True.
        False -> returns (divide_by, prefix) as a namedtuple,
                    with keys 'divide_by', 'prefix'.
        True  -> returns (divide_by, prefix, val in units of <prefix>) as a namedtuple,
                    with keys 'divide_by', 'prefix', 'value'
    more_units: list of tuples (divide_by, prefix)
        add these units to the list of known units, for this call of pick_units.
        for formatting & list of known_units, see parameter <units>.
    units: list of tuples (divide_by, prefix)
        This parameter defaults to the units & prefixes for:
            1e9, 1e6, 1e3, 1, 1e-3, 1e-6, 1e-9
        Example tuples: (1e9, 'G'), (1, ''), (1e-6, '$\\mu$')
    _weight: number between 0 and 1, inclusive. Default 0.05.
        Only used if value is an array. If value is an array:
        _weight tells how to weight min(v) and max(v) to pick a number to use for picking units.
            (v = abs(<value> with 0's removed).)
        When _weight is 1/2, use the midpoint; 0 -> use the min value; 1 -> use the max value.

    EXAMPLES
    --------
    pick_units(2000)
    >> UnitValue(divide_by=1e3, prefix='k', value=2).
    pick_units(8.2e+07)
    >> UnitValue(divide_by=1e6, prefix='M', value=8.2e+01)
    '''
    # check if value is an array. If so, use that array to decide what value to use to pick units.
    try:
        value[0]
    except Exception: #value is not an array
        pass
    else:             #value is an array
        value = np.array(value, copy=False)
        v = np.abs(value[value!=0.0])
        if len(v) == 0:
            return pick_units(0.0, return_value)
        v = (1 - _weight) * np.min(v)  +  _weight * np.max(v)
        unittuple = pick_units(v, False, more_units=more_units, units=units)
        if not return_value:
            return unittuple
        else:
            unit, prefix = unittuple
            return collections.namedtuple('UnitValue', ['divide_by', 'prefix', 'value'])(unit, prefix, value/unit)

    # add more_units to units; sort units from largest divide_by to smallest divide_by.
    units = units if type(units)==list else list(units)
    units += more_units                                     # append more_units
    units = sorted(units, key=lambda x: x[0], reverse=True) # smallest divide_by at end.
    
    # main work of function
    FOUND_UNIT = False
    for unit, prefix in units:
        if value >= unit:
            FOUND_UNIT = True
            break                # stop when unit is found!

    if not FOUND_UNIT and value==0.0:
        unit, prefix = (1, '')   # if value is 0, pick no units.

    # convert to namedtuple
    if not return_value:
        return collections.namedtuple('Unit',      ['divide_by', 'prefix'])(unit, prefix)
    else:
        return collections.namedtuple('UnitValue', ['divide_by', 'prefix', 'value'])(unit, prefix, value/unit)

pick_unit = pick_units    # alias

def picked_units(value, fmt_str='{value} {prefix}', **kw__pick_units):
    '''pick appropriate prefix for value; return fmt_string including this prefix.

    fmt_str: string
        will be hit by .format(value=value, prefix=prefix)

    return is based on whether value is a scalar or array:
        scalar --> return (fmt with value entered,   result of pick_units)
        array --> return (fmt without value entered, result of pick_units)

    Example:
        picked_units(2000, '{value} {prefix}m')
        # >> ('2 km', UnitValue(divide_by=1000.0, prefix='k', value=2.0))
    '''
    units = pick_units(value, **kw__pick_units)
    
    try:
        iter(value)
    except TypeError:   # value is single a number
        fmt_with_value = fmt_str.format(prefix=units.prefix, value=units.value)
        return (fmt_with_value, units)
    #else:               # value is an array
    try:
        fmt_without_value = fmt_str.format(prefix=units.prefix, value='{value}')        
    except ValueError:
        errmsg = "array will not be plugged in to fmt_str; instead will use .format(value='{value}')." +\
                 " Please use fmt_str with format code that accepts a string e.g. '{value:s}' or {value}" +\
                 " but not '{value:.2f}'. Also acceptable: fmt_str not containing {value} key."
        raise ValueError(errmsg)
    else:
        return (fmt_without_value, units)