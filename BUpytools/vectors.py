"""
Convenience for math with vectors.
"""

import numpy as np


''' ----------------------------- Angles ----------------------------- '''

def angle2d(a, b):
    '''returns angle from a to b.
    a and b should be 2d vectors or arrays of 2d vectors.
        if arrays of vectors, components should be on the last axis.
        i.e. a and b would have shape (..., 2)
    '''
    assert np.shape(a)[-1]==2, f"expected shape(a)=(..., 2), but got shape={np.shape(a)}"
    assert np.shape(b)[-1]==2, f"expected shape(b)=(..., 2), but got shape={np.shape(b)}"
    x_to_a = np.arctan2(a[..., 1], a[..., 0])
    x_to_b = np.arctan2(b[..., 1], b[..., 0])
    a_to_b = x_to_b - x_to_a
    return a_to_b
