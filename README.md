# BUpytools

Convenient codes by BU students for anyone.

See the [wiki](https://gitlab.com/Sevans7/BUpytools/-/wikis/home) for details about the existing BUpytools.

Have a tool/function/class you want to contribute to BUpytools? You could:
- send it in an email to [sevans7 @ bu.edu](https://gitlab.com/Sevans7), make sure to include "BUpytools" in the subject line
- OR, make a pull request for this repository with your changes (if you know how to do that)
- OR, request edit permissions of this repository by reaching out to [sevans7 @ bu.edu](https://gitlab.com/Sevans7)


## Getting Started. (Option 1 - Copy Paste)

The "simplest" option is to copy-paste the code you need directly from the python files in the repository (either [online](https://gitlab.com/Sevans7/BUpytools/-/tree/main/BUpytools) or locally from your machine after [installing BUpytools](#installing-bupytools)).

- This option is most useful if you may need to share your code and don't want to require other users of your code to install BUpytools.

- Recommend: put all the code you copy from BUpytools into a "tools.py" file in your project somewhere.

- While copying a function or class, remember to look at the top of the file for any relevant imports or defaults involved in the code you're copying. Sometimes there is a method which comes from elsewhere in BUpytools. For example, the `ProgressUpdatePrinter` class in [timing.py](https://gitlab.com/Sevans7/BUpytools/-/blob/main/BUpytools/timing.py) uses the `print_clear` method, which is imported at the top of timing.py from the [api.py](https://gitlab.com/Sevans7/BUpytools/-/blob/main/BUpytools/api.py) file. So, to appropriately copy-paste `ProgressUpdatePrinter` into your project, you also need to copy the `print_clear` method from `api.py`.

Note to direct contributors: when making edits to BUpytools, please attempt to keep the new tools "copy-pastable" in a similar manner. For example, DO: `from .api import print_clear` then call `print_clear`; DO NOT: `from . import api` then call `api.print_clear`.


## Getting Started. (Option 2 - Installation)

BUpytools can also work as a standalone package. Once you have followed the [installation](#installing-bupytools) steps below, getting started is as simple as:
```python
from BUpytools import strings as BUstr
BUstr.sci_notation(7654321)
>>> ' 7.654 \times 10^{6}'
```

- There are many other tools besides those in the `strings` file.

- See the [wiki](https://gitlab.com/Sevans7/BUpytools/-/wikis/home) or the [code](https://gitlab.com/Sevans7/BUpytools/-/tree/main/BUpytools) to explore the various tools.


## Installing BUpytools

```
cd desired_directory
git clone https://gitlab.com/Sevans7/BUpytools repo_name
cd repo_name
pip install -e .
```

Notes:
- `desired_directory` can be anywhere on your machine.
- `repo_name` can be any name you want; the BUpytools folder will go into `repo_name`. Recommend: `repo_name` = `BUpytools`.
- The `-e` tells to install in development mode, so if you make changes (locally, or via `git pull` after an update) they can be applied without re-installing.


## Authors and acknowledgment
Contributors:
- [Sevans7](https://gitlab.com/Sevans7)


## License
This project is licensed with the [Unlicense](https://gitlab.com/Sevans7/BUpytools/-/blob/main/LICENSE), which roughly means anyone can use the code in any form for free and without any acknowledgment required; copyrights are relinquished to the public domain; but don't hold the author(s) liable for any damages caused by use of this code.

If you are not comfortable contributing your tools under this license then we might be able to apply a different license to specific subsections of the code, or you can decide to keep the tools to yourself.

The spirit of BUpytools is to make tools that everyone can use to improve the Python programming experience.
