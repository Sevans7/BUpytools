"""
Created on Jun 17, 2022

@author: Sevans
"""

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
   long_description = fh.read()

setup(
      name     = 'BUpytools',
      version  = '0.1',
      author   = 'Samuel Evans',
      author_email = 'sevans7@bu.edu',
      long_description=long_description,
      long_description_content_type='text/markdown',
      url      = 'https://gitlab.com/Sevans7/BUpytools',
      packages = find_packages()
      )